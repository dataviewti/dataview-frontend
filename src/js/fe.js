$(document).ready(function(){
  
   $(window).on('load', function() { 
    $('.status').fadeOut();
    $('.preloader').delay(350).fadeOut('slow'); 
  }); 

  /* configure scrollspy bs4 */
  $('#navbar-menu').attr('data-scroll',60);
  $('body').scrollspy({
     target: '#navbar-menu',
     offset:parseInt($('#navbar-menu').attr('data-scroll'))+10, 
  });

  $(window).on('activate.bs.scrollspy', function (){
    if(!$('.nav.simple-menu li').hasClass('active-static'))
      $('.nav.simple-menu li').removeClass('active').find('a.active').parent().addClass('active');
  });

  $('.float-panel').attr('data-top',parseInt($('.float-panel').offset().top));

  $(window).scroll(function(){
    if($(this).scrollTop() >= $('.float-panel').attr('data-top') && !$('.float-panel').hasClass('fixed')){
      $('.float-panel').addClass('fixed fixed-top').removeClass('no-fixed').css({top:0});
      //console.log($('.float-panel').offset().top);
      //$('body').css({'padding-top':$('.float-panel').height()})//to prevent collapse for empty space 
    }
		else{
			if($(this).scrollTop() <= $('.float-panel').attr('data-top') && $('.float-panel').hasClass('fixed')){  
        $('.float-panel').css({top:$('.float-panel').attr('data-top')}).removeClass('fixed fixed-top').addClass('no-fixed');
        //$('body').css({'padding-top':0})
			}
		}
  });

	//smoot scroll
	
	/*$(".scroll").click(function(event){
    event.preventDefault();
		$('html,body').animate({
			scrollTop:($(this.hash).offset().top - parseInt($('#navbar-menu').attr('data-scroll'))),
		}, {
				duration: 1500,
				easing: 'easeInOutExpo',
				complete: function(){
			}
		})
  });*/

	//smoot scroll without jquery.ui
  $('.scroll').click(function(event) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1800, function() {
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus"))
            return false;
          else {
            $target.attr('tabindex','-1');
            $target.focus();
          };
        });
      }
  });


  $("#back-to-top").hide();
  $(function () {
    var wh = $(window).height();
    var whtml =  $(document).height();
    $(window).scroll(function () {
      if ($(this).scrollTop() > whtml/10) {
        $('#back-to-top').fadeIn();
      } else {
        $('#back-to-top').fadeOut();
      }
    });
    $('#back-to-top').click(function () {
      $('body,html').animate({
        scrollTop: 0
      }, 800);
      return false;
    });
  });

});
