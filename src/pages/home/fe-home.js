var SPMaskBehavior = function (val){return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009'};

$.ajaxSetup({
  dataType: "json",
  method:'GET',
  headers: {'X-CSRF-Token': laravel_token},
  async: true,
});

swal.setDefaults({
  buttonsStyling:false,
  confirmButtonClass:"btn btn-lg btn-raised btn-primary btn-swal-custom-intranetone",
  cancelButtonClass:"btn btn-lg btn-raised btn-warning btn-swal-custom-intranetone",
  buttonsStyling:false,
  confirmButtonText: "<span class = 'fa fa-thumbs-up'></span> Sim",
  cancelButtonText: "<span class = 'fa fa-thumbs-down'></span> Não",
  showCancelButton: false,
  reverseButtons:false,
  allowOutsideClick:false,
});

/** Pickadate default values */
$.extend($.fn.pickadate.defaults, {
  format: 'dd/mm/yyyy',
  formatSubmit: 'yyyy-mm-dd',
  labelMonthNext: 'Avançar para o próximo mês',
  labelMonthPrev: 'Voltar ao mês anterior',
  labelMonthSelect: 'Selecione um mês da lista',
  labelYearSelect: 'Selecione um ano da lista',
  selectMonths: true,
  selectYears: true
});

$(document).ready(function($){
  
  //pickadate objects initialization
  $('#dtini').pickadate({
    formatSubmit: 'yyyy-mm-dd 00:00:00',
    onClose:function(){
      //$("[name='rg']").focus();
      console.log('asas');
    }
  }).pickadate('picker').on('render', function(){
    reserva_form.formValidation('revalidateField', 'dtini');
  });

  $('#dtfim').pickadate({
    formatSubmit: 'yyyy-mm-dd 00:00:00',
    onClose:function(){
      //$("[name='rg']").focus();
    }
  }).pickadate('picker').on('render', function(){
    reserva_form.formValidation('revalidateField', 'dtfim');
  });


  $("#phone").mask(SPMaskBehavior,{
    //placeholder:'(__) ____-____',
    onComplete: function(obj,event,currentField){},     
    onKeyPress: function(val, e, field, options){
      field.mask(SPMaskBehavior.apply({}, arguments), options);
      reserva_form.formValidation('revalidateField', 'phone');
    }
  });
  
  $("#cont_phone").mask(SPMaskBehavior,{
    placeholder:'(__) ____-____',
    onComplete: function(obj,event,currentField){},     
    onKeyPress: function(val, e, field, options){
      field.mask(SPMaskBehavior.apply({}, arguments), options);
      contato_form.formValidation('revalidateField', 'cont_phone');
    }
  });  

  
   //FormValidation initialization
   reserva_form = $('#reserva-form').formValidation({
    locale: 'pt_BR',
    framework: 'bootstrap',
    icon: {
      valid: 'fv-ico ico-check',
      invalid: 'fv-ico ico-close',
      validating: 'fv-ico ico-gear ico-spin'
    },
    excluded: ':disabled',
    fields:{
      'name':{
        validators:{
          notEmpty:{
            message: 'O nome é obrigatório!'
          }
        }
      },
      dtini:{
        validators:{
          notEmpty:{
            message: 'Informe a data de entrada!'
          },
          date:{
            format: 'DD/MM/YYYY',
            message: 'Informe uma data válida!'
          }
        }
      },
      dtfim:{
        validators:{
          notEmpty:{
            message: 'Informe a data de saída!'
          },
          date:{
            format: 'DD/MM/YYYY',
            message: 'Informe uma data válida!'
          }
        }
      },
      mail:{
        validators:{
          notEmpty:{
            message: 'O email é obrigatório!'
          },
          mail:{
            message: 'email inválido!'
          }
        }
      },
      phone:{
        validators:{
          notEmpty:{
            message: 'O telefone é obrigatório!'
          },
          phone:{
            country: 'BR',
            message: 'telefone inválido!'
          }
        }
      },
    }
  }).on('err.field.fv', function(e, data){
  }).on('success.form.fv', function(e){
    e.preventDefault();
    var $form = $(e.target);
    
    console.log($form.serializeArray());
    $.ajax({
      url:'/mailer/pre-reserva',
      method:"POST",
      type:'POST',
      data:$form.serializeArray(),
      dataType: "json",
      headers: {'X-CSRF-Token': laravel_token},
      beforeSend:function(){
        HoldOn.open({message:"Enviando dados, aguarde...",theme:'sk-bounce'});
      },
      success: function(data){
        if(data.message == 'sucesso'){
          HoldOn.close();
          swal({
            title:"Solicitação enviada com sucesso, em breve entraremos em contato!",
            confirmButtonText:'OK',
            type:"success",
            onClose:function(){
              $('#reserva-form')[0].reset();
            }
          });
        }
      },
      error: function(data){
        console.log('err',data);
        HoldOn.close();
        swal({
          title:"Ocorreram erros, a solicitação não pode ser enviada, tente novamente mais tarde, ou entre em contato pelo email contato@aguasdojalapao.com.br!",
          confirmButtonText:'OK',
          type:"error",
          onClose:function(){
            $('#reserva-form')[0].reset();
          }
        });
      },
      done:function(){
        HoldOn.close();
      }
    });

  });






  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  });
  console.log('asas');
  $('#top-slides').carousel({
    interval: 6000
  });
  new WOW().init();
});
